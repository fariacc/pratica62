//import java.lang.Math;
import java.util.List;
import java.util.ArrayList;
import utfpr.ct.dainf.if62c.pratica.Time;
import utfpr.ct.dainf.if62c.pratica.Jogador;
import utfpr.ct.dainf.if62c.pratica.JogadorComparator;


public class Pratica62
{
  public static void main(String[] args)
  {
    Time time1 = new Time();
    Time time2 = new Time();

    time1.addJogador("Goleiro", new Jogador(1, "Tafarel"));
    time1.addJogador("Lateral", new Jogador(13, "Jose"));
    time1.addJogador("Atacante", new Jogador(14, "Helena"));

    time2.addJogador("Goleiro", new Jogador(1, "Joao"));
    time2.addJogador("Lateral", new Jogador(5, "Mario"));
    time2.addJogador("Atacante", new Jogador(3, "Sandra"));

    //time1.compara(time2);
    
    List<Jogador> jogadores_1 = new ArrayList<Jogador>();
    List<Jogador> jogadores_2 = new ArrayList<Jogador>();
    
    jogadores_1 = time1.ordena(new JogadorComparator(true, true, false));
    jogadores_2 = time2.ordena(new JogadorComparator(true, true, false));
      
    for(int i = 0 ; i < 3 ; i++)
      System.out.println(jogadores_1.get(i).nome + " " + jogadores_1.get(i).numero);
    for(int i = 0 ; i < 3 ; i++)
      System.out.println(jogadores_2.get(i).nome + " " + jogadores_2.get(i).numero);
  }
}